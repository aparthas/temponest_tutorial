"""
A script which performs posterior re-weighting and generates corner plots
for TempoNest runs.
"""
from __future__ import division
import numpy as np
import pandas as pd
import argparse
import re
import os
import csv

import matplotlib.pyplot as plt
import corner

def post_processing(psr_name, path, n_params):
    """
    Create plots of the posterior distrbutions and save the samples in a more friendly format.
    """
    param_names = os.path.join(path, "{0}-.paramnames".format(psr_name))
    param_file = os.path.join(path, "{0}-".format(psr_name))
    data_file = os.path.join(path, "{0}-post_equal_weights.dat".format(psr_name))

    col_names=[]
    with open (param_names,'r') as param:
        for line in param:
            print(line)
            sline = line.strip().split(" ")
            if len (sline) > 1:
                col_names.append(sline[1])
            else:
                col_names.append(re.split('(\d+)',sline[0])[-1])
    param.close()

    args_cols = [i for i, e in enumerate(col_names)]
    mean = [0] * len(col_names)
    sigma = [1] * len(col_names)

    data = np.genfromtxt(data_file)

    print("Rescaling")
    scaling_file  = str(os.path.join(path, "{0}-T2scaling.txt".format(psr_name)))

    if( not os.path.exists(scaling_file) ) :
        print("no scaling file. are you sure the run is over?")

    with open(scaling_file, 'r') as f:
        for line in f:
            chunks = line.strip().split(" ")
            name = chunks[0]
            m = 0
            s = 1
            if name in col_names:
                if len(chunks) == 3:
                    m = float(chunks[1])
                    s = float(chunks[2])
                elif len(chunks) == 4:
                    m = float(chunks[2])
                    s= float(chunks[3])
                else:
                    print("invalid chunk:",chunks)
                index = col_names.index(name)
                mean[index] = m
                sigma[index] = s
            else:
                print("skipping unused column",name)

    samples = []
    if n_params != 0:
        for i in range(n_params):
            samples.append(mean[i]+sigma[i]*data[:,i])
    else:
        pass

    for i in range(4):
        par = 4 - i
        samples.append(mean[-(par)]+sigma[-(par)]*data[:,-(par+1)])

    samples = np.transpose(samples)

    col_names = [col_names[i] for i in args_cols]
    pd.DataFrame(samples).to_csv("{0}/{1}_tempo_posteriors.csv".format(path, psr_name), 
                                  header=col_names)

    corner.corner(samples, bins=50, labels=col_names)
    plt.savefig(os.path.join(path,"{0}_corner.png".format(psr_name)))
    plt.show()

# ----------------------------------------------------------------------------------
# Input arguments
parser = argparse.ArgumentParser(description="Generate noise models using TEMPONEST")
parser.add_argument("-p", dest="psrname", help="Name of the pulsar")
parser.add_argument("-n", dest="n_param", help="Number of custom parameters fit for")
parser.add_argument("-o", dest="path", help="Path to TempoNest output")
args=parser.parse_args()

n_params = int(args.n_param)
path = str(args.path)

# Run post-processing:
post_processing(args.psrname, path, n_params)
