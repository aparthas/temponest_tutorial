# TempoNest_tutorial

Tutorial on using TempoNest.
1. Example ephemerides and ToA files. 
2. Simulated ToAs with various induced signatures. 
3. Well commented configuration files for TN. 
4. Basic usage.
5. Demonstrate model selection. 
6. Script for generating posterior distributions from TN results.